﻿using System;
using System.Threading;

namespace ThreadException
{
    internal class Program
    {
        private static Calc calc = new Calc();

        private class Calc
        {
            public int a;
            public int b;

            private static readonly Random random = new Random();

            public void Divide()
            {
                for (int i = 0; i < 100000; i++)
                {
                    a = random.Next(1, 10);
                    b = random.Next(1, 10);
                    try
                    {
                        int result = a / b;
                    }
                    catch(Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    a = 0;
                    b = 0;
                }
            }
        }

        private static void Main(string[] args)
        {
            Thread t1 = new Thread(calc.Divide);
            t1.Start();
            calc.Divide();
        }
    }
}